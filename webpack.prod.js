const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
	mode: 'production',

	plugins: [
		new CleanWebpackPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('production'),
			}
		}),
		new CopyWebpackPlugin([
			{
				from: path.resolve(__dirname, 'manifest', '**', '*'),
				to: path.resolve(__dirname, 'build')
			},
		]),
	]
}