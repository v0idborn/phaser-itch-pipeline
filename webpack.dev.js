const path = require('path')
const webpack = require('webpack')

module.exports = {
	mode: 'development',
	devtool: 'eval-source-map',

	devServer: {
		contentBase: path.resolve(__dirname, 'build/'),
		writeToDisk: true
	},

	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('development'),
			}
		}),
	]
}