# phaser-itch jam pipeline!

Hi! This is working automation to quickly develop games (in Phaser) and easily upload (to itch.io).
This configuration bootstraps you to just start quickly developing games and then upload them to itch - in one command at a time

*note*: this is quickie and dirty **POC** created for **Ludum Dare 45**, issues may arise.

## Bootstrap!
### First steps
Prepare your environment by installing [nodejs](https://nodejs.org/en/download/), [butler](https://itch.io/docs/butler/installing.html) and register in [itch.io](https://itch.io/register). Then clone this repo and use good ol' ``npm install``

*note*: If you want to use it in LD, you have to also register in [Ludum Dare](https://ldjam.com/#user-register)

### Integrate Itch.io
Create new project in itch.io (in web browser, yuck!) - it won't work otherwise. Be sure to follow these [easy steps](https://itch.io/docs/creators/html5). Most important part is setting how game will be embedded. I know, this process sucks, but at least it's quick and one first time per project only.

### Change variables in package.json
```
"name": "YOUR GAME NAME",
  "config" : {
    "itch_user" : "YOUR NAME" ,
    "itch_project" : "YOUR GAME NAME"
  },
  "author": "YOUR STREET NAME",
```
## Usage
1. Put assets in ``./assets`` folder
2. Put your code in ``./src`` folder
3. Update your ``./index.html`` file (note, that ``*.js`` files will link automatically)

### Commands
| command 		     | explanation           |
| -------------------:|:---------------------|
| ``npm run serve``      | bundles, serves game on localhost:8080 and opens browser |
| ``npm run build``      | bundles *'production ready'* package to build folder |
| ``npm run upload``     | lazily uploads changed game elements to itch.io |

*note*: Before you'll be able to upload game, you have to login to itch.io using ``butler.login`` - you can easily find how to do that with API_KEY if you want.

## Contributing
Pull requests are welcome. Issues also. It's nothing I would deem production ready, but it's a better start than nothing. Just please use Angular convention for git messages.

## License
[MIT](https://choosealicense.com/licenses/mit/)