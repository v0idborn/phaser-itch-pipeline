const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
	devtool: 'eval-source-map',

	entry: {
		app: './src/app.js',
    },

    output: {
		path: path.resolve(__dirname, 'build'),
		filename: '[name].bundle.js',
	},
    
	module: {
		rules: [
			{
				test: /\.js$/,
				include: path.resolve(__dirname, 'src/'),
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			}
		]
	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				phaser: { test: /[\\/]node_modules[\\/]((phaser).*)[\\/]/, name: "phaser", chunks: "all" },
			}
		}
	},

	plugins: [
		new CopyWebpackPlugin([
			{
				from: path.resolve(__dirname, 'assets', '**', '*'),
				to: path.resolve(__dirname, 'build')
			},
		]),
		new HtmlWebpackPlugin({
			template: path.resolve(__dirname, 'index.html')
		}),
		new webpack.DefinePlugin({
			'typeof CANVAS_RENDERER': JSON.stringify(true),
			'typeof WEBGL_RENDERER': JSON.stringify(true)
		}),
	]
}